import requests
import simplejson as json

# The port is 5070 by default
api_url = 'localhost:5090'
database = 'DEMO60'

api = '/'.join(['http:/', api_url, database])
ctx = {
    'company': 1,
    'user': 1,
    'shop': 1,  # id de Sucursal / Almacen / Tienda
}

args = {
    'party': {
        'id': 17,
        'name': 'JUAN MANUEL',  # optional
        'id_number': '13547322',  # optional
    },
    'shipment_date': '2021-05-17',
    'description': 'VENTA DE PRUEBA API',
    'lines': [
        {
            'product': 23,  # id of product
            'quantity': 1,  # integer / float
            'unit_price': 5000,
        }, {
            'product': 50,
            'quantity': 3,
            'unit_price': 3800,
        }
    ],
}


# This method create a Sale Order (Este metodo crea una venta)
body = {
    'model': 'sale.sale',
    'method': 'dash_quote',
    'args': args,
    'context': ctx,
}

data = json.dumps(body)

# Test for trigger a remote transaction
route = api + '/model_method'
result = requests.post(route, data=data)

print(result)
for k, v in result.json().items():
    print(k, ' : ', v)
