import logging
import base64
from decimal import Decimal
import simplejson as json

from flask import Flask, request, abort, jsonify
from flask_tryton import Tryton
from flask.globals import current_app
from flask_cors import CORS
from tools import (
    send_data, get_config, InvalidAPIUsage, CustomJSONEncoder, print_rec
)

trytond_config = get_config('trytond_config')

OPERATION_CENTER = {
    "8": '001',
    "12": '002',
    "23": '006',
    "18": '001',
    "10": '200',
    "11": '300',
    "13": '003',
    "17": '004',
    "19": '100',
    "20": '201',
    "14": '001',
    "21": '005',
    "22": '001',
}


# def print_rec(rec):
#     for k, j in rec.items():
#         print(k, ' : ', j)
#
#
# def is_class(obj):
#     if obj.get('__class__'):
#         # FIXME: Add datetime, bytes, decimal, too
#         if obj['__class__'] == 'date':
#             return date(obj['year'], obj['month'], obj['day'])
#     return obj
#
#
# def get_data(decode=False):
#     if request.args:
#         # From react web
#         data = request.args.to_dict()
#         if data.get('context'):
#             data['context'] = eval(data['context'])
#         return data
#     elif hasattr(request, 'data') and request.data != b'':
#         data = request.data.decode("utf-8")
#         return json.loads(data, object_hook=is_class)
#     else:
#         try:
#             return request.json
#         except AttributeError:
#             logging.warning('Attribute error unknown...!')
#
#
# def truncate(number, digits) -> float:
#     stepper = 10.0 ** digits
#     return math.trunc(stepper * number) / stepper
#
#
# class InvalidAPIUsage(Exception):
#     status_code = 400
#
#     def __init__(self, message, status_code=None, payload=None):
#         super().__init__()
#         self.message = message
#         if status_code is not None:
#             self.status_code = status_code
#         self.payload = payload
#
#     def to_dict(self):
#         rv = dict(self.payload or ())
#         rv['message'] = self.message
#         return rv
#
#
# class CustomJSONEncoder(JSONEncoder):
#
#     def default(self, obj):
#         try:
#             if isinstance(obj, types.ModuleType):
#                 return str(obj)
#             elif isinstance(obj, types.MethodType):
#                 return str(obj)
#             elif isinstance(obj, date):
#                 return obj.isoformat()
#             elif isinstance(obj, 'bytes'):
#                 return base64.b64encode(obj)
#             iterable = iter(obj)
#         except TypeError:
#             pass
#         else:
#             return list(iterable)
#         return JSONEncoder.default(self, obj)


def create_app(dbname):
    app = Flask(dbname, instance_relative_config=False)
    CORS(app)
    app.app_context().push()  # ???
    try:
        with app.app_context():
            current_app.json_encoder = CustomJSONEncoder
            current_app.config['TRYTON_DATABASE'] = dbname
            current_app.config['TRYTON_CONFIG'] = trytond_config
            current_app.config['CORS_HEADERS'] = 'Content-Type'
            tryton = Tryton(current_app)
            _pool = tryton.pool
    except:
        logging.warning(
            'Error database disabled or unknown error: %s' % dbname)
        return None
    # _models = MODELS.copy()

    # @app.errorhandler(500)
    # def resource_not_found(e):
    #     status = 'error'
    #     msg = 'api_error_unknown'
    #     return jsonify(status=status, msg=msg), 500

    @app.errorhandler(InvalidAPIUsage)
    def invalid_api_usage(e):
        return jsonify(e.to_dict())

    @app.route('/')
    @tryton.transaction()
    def home():
        Company = _pool.get('company.company')
        company, = Company.search_read([
            ('id', '=', 1)
        ], fields_names=['party.name'])

        msg = 'Hello welcome to %s' % company['party.name']
        return send_data(msg)

    # --------------------ENDPOINTS SHOPIFY--------------------------

    @app.route("/notification/shopify", methods=['POST'])
    @tryton.transaction()
    def notification_shopify():
        Channel = _pool.get('sale.web_channel.shopify')
        res = Channel.request_api(request)
        return '...', 200

    # --------------------ENDPOINTS MERCADOLIBRE --------------------------
    @app.route("/notification/mercadolibre", methods=['POST'])
    @tryton.transaction()
    def notification_mercado_libre():
        req = request.data.decode("utf-8")
        file = request.data
        data = json.loads(req)
        if not data:
            return send_data([{}])

        def create_sale(data, req):
            Channel = _pool.get('sale.web_channel.mercado_libre')
            ApiLog = _pool.get('api.log.channel')
            Date = _pool.get('ir.date')
            channel = Channel._get_channel()
            error = ''
            try:
                value = {
                  'channel': channel,
                  'record_date': Date.today(),
                  'request_json': data,
                  'file_json': file,
                  'status': 'pending'}
                ApiLog.create([value])
                return {'ok': 'Success process Log'}
            except Exception as error:
                print('Caught this error: ' + repr(error))
                channel_ = Channel._get_channel()
                channel_.send_mail_notification(
                  error.message + ' in resource \n' + str(data))
                return {'error': 'Fail process in server'}
            return res

        res = create_sale(data, req)
        return send_data(res)

    @app.route("/login_app/mercadolibre", methods=['GET'])
    @tryton.transaction()
    def login_app_mercado_libre():
        print(request, 'this is data')
        return {'status': 'ok', 'message': "success login"}

    @app.route("/process_invoice", methods=['POST'])
    def process_invoice():
        data = json.loads(request.data.decode("utf-8"))
        ctx = data.get('context', {})
        msg = {'status': 'ok'}
        user = ctx.get('user')

        @tryton.transaction(context=ctx, user=user)
        def _process_invoice():
            Invoice = _pool.get('account.invoice')
            invoices = Invoice.search([
                ('state', '=', 'validate'),
                ('id', '>=', '107517'),
            ], order=[('create_date', 'ASC')])
            for ord in invoices:
                try:
                    Invoice.submit([ord])
                except:
                    pass
            return True

        try:
            res = _process_invoice()
            if not res:
                msg['status'] = 'error'
        except Exception as e:
            print(e)
            abort(500, description="Error in API transaction")

        return send_data(msg)

    # @app.route("/execute", methods=['POST'])
    # def execute():
    #     """
    #     Execute a tryton model method
    #     """
    #     data = get_data()
    #     model = data.get('model', None)
    #     method = data.get('method', None)
    #     ctx = data.get('context', {})
    #     args = data.get('args', {})
    #     if isinstance(args, list) and args:
    #         args = args[0]
    #     elif isinstance(args, str) and args:
    #         args = json.loads(args)
    #
    #     if isinstance(ctx, str) and ctx:
    #         ctx = json.loads(ctx)
    #
    #     user = ctx.get('user') or 0
    #
    #     @tryton.transaction(context=ctx, user=user)
    #     def _execute():
    #         Model = _pool.get(model)
    #         _method = getattr(Model, method)
    #         if args:
    #             if isinstance(args, dict):
    #                 res = _method(args, ctx)
    #             else:
    #                 logging.warning('Using deprecation option, please fix it!')
    #                 res = _method(**args)
    #         else:
    #             res = _method()
    #         return res
    #
    #     res = _execute()
    #     return send_data(res)
    #
    # -------------- ENDPOINTS PASTEUR ----------------

    def jsonify_record(record, fields=None, binaries=None):
        res_fields = {}
        if not fields:
            fields = ['id', 'name', 'rec_name']
        else:
            fields.append('rec_name')
        for fd in fields:
            if '.' in fd:
                target = fd.split('.')
                _field = target.pop(0)
                next_field = '.'.join(target)
                if _field not in res_fields.keys():
                    res_fields[_field] = []
                res_fields[_field].append(next_field)
            else:
                res_fields[fd] = []

        _record_data = {'id': record.id}

        for f in res_fields:
            try:
                value = getattr(record, f, None)
            except:
                logging.warning('Attribute field unknown > ',
                                f, record.__name__)
                _record_data[f] = None
                continue
            if value is None:
                _record_data[f] = None
                continue
            elif hasattr(value, 'id'):
                _record_data[f] = {
                    'id': value.id
                }
                if hasattr(value, 'rec_name'):
                    _record_data[f]['name'] = value.rec_name
                elif hasattr(value, 'name'):
                    _record_data[f]['name'] = value.name

                child_fields = res_fields.get(f)
                if child_fields:
                    _record_data[f] = jsonify_record(value, child_fields)
            elif value and isinstance(value, tuple):
                try:
                    sub_model = value[0].__name__
                    sub_fields = _models[sub_model]['fields']
                    sub_elements = [(jsonify_record(e, sub_fields))
                                    for e in value]
                    _record_data[f] = sub_elements
                except:
                    sub_fields = res_fields[f]
                    sub_elements = [(jsonify_record(v, sub_fields))
                                    for v in value]
                    _record_data[f] = sub_elements
            elif value and isinstance(value, list):
                # Flask require that lists are dumped before send
                _record_data[f] = json.dumps(value)
            elif value and isinstance(value, time):
                # Flask require that lists are dumped before send
                _record_data[f] = str(value)
            else:
                _record_data[f] = value

        if binaries:
            for fb in binaries:
                val = getattr(record, fb, None)
                if val:
                    _record_data[fb] = base64.b64encode(val)
                else:
                    _record_data[fb] = None
        return _record_data

    def __create_party(_pool, rec):
        Party = _pool.get('party.party')
        City = _pool.get('party.city_code')
        Department = _pool.get('party.department_code')
        Country = _pool.get('party.country_code')
        Obligation = _pool.get('party.obligation_fiscal')
        Category = _pool.get('party.category')
        address = rec.pop('address')
        country, = Country.search([
            ('code', '=', address['country_code']),
        ])
        address['country_code'] = country.id
        city, = City.search([
            ('code', '=', address['city_code']),
            ('department.code',  '=', address['department_code']),
        ])
        address['city_code'] = city.id
        department, = Department.search([
            ('code',  '=', address['department_code']),
        ])
        address['department_code'] = department.id
        address['street'] = address.get('street').upper()
        rec['name'] = rec['name'].upper()
        rec['addresses'] = [('create', [address])]
        obligations = Obligation.search([
            ('code', '=', rec.pop('obligaciones_fiscales')),
        ])
        if obligations:
            rec['party_obligation_tax'] = [('add', [obligations[0].id])]
        else:
            rec['party_obligation_tax'] = [('add', [116])]
        rec['contact_mechanisms'] = [('create', [
            {'type': 'mobile', 'value': rec.pop('phone', '')},
            {'type': 'email', 'value': rec.pop('email', '')}
        ])]
        _ = rec.pop('is_person', None)
        _ = rec.pop('new_id_number', None)
        category_code = rec.pop('category', None)
        if category_code:
            categories = Category.search([('code', '=', category_code)])
            if categories:
                rec['categories'] = [('add', [categories[0].id])]
        try:
            print('rec...........', rec)
            _record, = Party.create([rec])
            _record.on_change_type_person()
        except Exception as e:
            return InvalidAPIUsage('this is exception ' + e, status_code=404)
        return _record

    # Dos endpoints creacion tercero actualizacion validacion de digito de
    # verificacion cuando se crea una empresa
    @app.route("/create_party", methods=['POST'])
    def create_party():
        data = json.loads(request.data.decode("utf-8"))
        rec = data['record']
        ctx = data.get('context', {})
        msg = {'status': 'ok'}
        user = None
        print_rec(rec)
        if ctx:
            user = ctx.get('user')
        else:
            try:
                ctx = {'company': 1, 'user': 1}
                user = 1
            except:
                logging.warning('Missing web user...!')

        @tryton.transaction(context=ctx, user=user)
        def _create_party():
            Party = _pool.get('party.party')
            dom = [('id_number', '=', rec['id_number'])],
            parties = Party.search(dom)
            if parties:
                party = parties[0]
                record = {
                    'party': party.id_number,
                    'ext_id': party.ext_id,
                    'response': 'Tercero ya existe!',
                    'status': 'error',
                }
                return record
            else:
                _record = __create_party(_pool, rec)
            _fields = ['ext_id', 'name', 'id_number']
            json_res = jsonify_record(_record, _fields)
            return json_res

        try:
            record = _create_party()
        except:
            abort(500, description="Error in API transaction")
        if record.get('status'):
            msg['status'] = record.pop('status')
        msg['record'] = record
        return send_data(msg)

    @app.route("/update_party", methods=['POST'])
    def update_party():
        data = json.loads(request.data.decode("utf-8"))
        rec = data['record']
        ctx = data.get('context', {})
        msg = {'status': 'ok'}
        user = None
        print_rec(rec)
        if ctx:
            user = ctx.get('user')
        else:
            try:
                ctx = {'company': 1, 'user': 1}
                user = 1
            except:
                logging.warning('Missing web user...!')

        @tryton.transaction(context=ctx, user=user)
        def _update_party():
            Party = _pool.get('party.party')
            City = _pool.get('party.city_code')
            Department = _pool.get('party.department_code')
            Country = _pool.get('party.country_code')
            Obligation = _pool.get('party.obligation_fiscal')
            Address = _pool.get('party.address')
            Category = _pool.get('party.category')

            dom = [('id_number', '=', rec['id_number'])]
            parties = Party.search(dom)
            party = None
            if parties:
                party = parties[0]
                if rec.get('address'):
                    address = rec.pop('address')
                    address['street'] = address.get('street').upper()
                    if address.get('country_code'):
                        country, = Country.search([
                            ('code', '=', address['country_code']),
                        ])
                        address['country_code'] = country.id

                    if address.get('department_code'):
                        department, = Department.search([
                            ('code',  '=', address['department_code']),
                        ])
                        address['department_code'] = department.id

                    if address.get('city_code'):
                        city, = City.search([
                            ('code', '=', address['city_code']),
                            ('department',  '=', department.id),
                        ])
                        address['city_code'] = city.id
                    if party.addresses:
                        _address = party.addresses[0]
                    Address.write([_address], address)

                phone = rec.pop('phone', None)
                email = rec.pop('email', None)
                try:
                    if phone or email:
                        for contact in party.contact_mechanisms:
                            update = False
                            if contact.type in ['mobile', 'phone'] and (
                                    phone or phone == 0):
                                contact.value = str(phone)
                                update = True
                            elif contact.type == 'email' and email:
                                contact.value = email
                                update = True
                            if update:
                                contact.save()
                except:
                    pass

                rec['name'] = rec['name'].upper()

                if rec.get('new_id_number'):
                    rec['id_number'] = rec.pop('new_id_number')
                obligation_tax = rec.pop('obligaciones_fiscales', None)
                category_code = rec.pop('category', None)
                if category_code:
                    categories = Category.search(
                        [('code', '=', category_code)])
                    if categories:
                        rec['categories'] = [('add', [categories[0].id])]
                if obligation_tax:
                    obligations = Obligation.search([
                        ('code', '=', obligation_tax),
                    ])
                    if not obligations:
                        record = {
                            'obligacion': obligation_tax,
                            'response': 'Obligación no válida!',
                            'status': 'error',
                        }
                        return record

                    obligation = obligations[0]
                    if party.party_obligation_tax:
                        for of in party.party_obligation_tax:
                            if of.id != obligation.id:
                                Party.write([party], {'party_obligation_tax': [
                                    ('remove', [of.id]),
                                    ('add', [obligation.id])
                                ]})
                    else:
                        rec['party_obligation_tax'] = [
                            ('add', [obligation.id])
                        ]

                print('======', rec)
                Party.write([party], rec)
                party.on_change_type_person()
                _record = {'id_number': party.id_number}
                return _record
            else:
                _record = __create_party(_pool, rec)
                _fields = ['ext_id', 'name', 'id_number']
            json_res = jsonify_record(_record, _fields)
            return json_res

        if 1:  # try:
            record = _update_party()
        else:  # except:
            abort(500, description="Error in API transaction")

        if record.get('status') == 'error':
            msg['response'] = record.pop('response')
            msg['status'] = record.pop('status')
        msg['record'] = record
        return send_data(msg)

    @app.route("/save_test_type", methods=['POST'])
    def save_test_type():
        data = json.loads(request.data.decode("utf-8"))
        rec = data['record']
        ctx = data.get('context', {})
        msg = {'status': 'ok'}
        user = None
        print_rec(rec)
        if ctx:
            user = ctx.get('user')
        else:
            # In web forms there isn't context, we need to keep from
            # to search webuser in database
            # User = _pool.get('res.user')
            try:
                ctx = {'company': 1, 'user': 1}
                user = 1
            except:
                logging.warning('Missing web user...!')

        @tryton.transaction(context=ctx, user=user)
        def _save_test_type():
            Product = _pool.get('product.template')
            products = Product.search([
                ('code', '=', rec['code']),
            ])
            product = None
            if products:
                product = products[0]

            if product:
                to_save = {
                    'name': rec['name'].upper(),
                }
                Product.write([product], to_save)
                return product
            else:
                to_save = {
                    'name': rec['name'].upper(),
                    'type': 'service',
                    'cost_price_method': 'fixed',
                    'default_uom': 1,
                    'sale_uom': 1,
                    'salable': True,
                    'list_price': 0,
                    'active': True,
                    'code': rec['code'],
                    'reference': rec.get('cups_code'),
                    'account_category': 1,
                    'products': [('create', [{
                        'cost_price': 1,
                    }])]
                }
                try:
                    _record, = Product.create([to_save])
                    return _record
                except Exception as e:
                    msg['status'] = 'error'
                    msg['msg'] = e
                    print(e)

        try:
            record = _save_test_type()
            if record:
                _fields = ['code', 'id', 'name']
                json_res = jsonify_record(record, _fields)
                msg['record'] = json_res
                return json_res
        except Exception as e:
            abort(500, description="Error in API transaction")

        return send_data(msg)

    @app.route("/save_lab_order", methods=['POST'])
    def save_lab_order():
        data = json.loads(request.data.decode("utf-8"))
        rec = data['record']
        ctx = data.get('context', {})
        msg = {'status': 'ok'}
        user = ctx.get('user')
        print_rec(rec)

        @tryton.transaction(context=ctx, user=user)
        def _save_lab_order():
            LabOrder = _pool.get('laboratory.order')
            Party = _pool.get('party.party')
            Template = _pool.get('product.template')
            PaymentTerm = _pool.get('account.invoice.payment_term')
            PaymentMode = _pool.get('account.voucher.paymode')
            OperationCenter = _pool.get('company.operation_center')
            orders = LabOrder.search(['number', '=', rec['number']])
            if not orders:
                # Add is_person
                parties = Party.search([
                    ('id_number', '=', rec['customer']['id_number']),
                ])
                if parties:
                    party = parties[0]
                else:
                    msg['status'] = 'error'
                    msg['msg'] = 'Tercero no encontrado'
                    return
                if rec.get('patient'):
                    if rec['customer'] != rec['patient']:
                        patients = Party.search([
                            ('id_number', '=', rec['patient']['id_number']),
                        ])
                        if patients:
                            patient = patients[0]
                        else:
                            msg['status'] = 'error'
                            msg['msg'] = 'Paciente no encontrado'
                            return
                    else:
                        patient = party
                else:
                    patient = None

                oc_code = OPERATION_CENTER[rec['operation_center']]
                operation_centers = OperationCenter.search([
                    ('code', '=', oc_code)
                ])
                if not operation_centers:
                    msg['status'] = 'error'
                    msg['msg'] = 'Centro de operaciones no encontrado'
                    return

                payment_terms = PaymentTerm.search([
                    ('code', '=', rec['payment_term'])
                ])
                if not payment_terms:
                    code = rec['payment_term']
                    msg['status'] = 'error'
                    msg['msg'] = f'El plazo de pago no existe {code}'
                    return

                payment_term = payment_terms[0]
                _ = rec.pop('list_price_code')
                rec['customer'] = party.id
                rec['company'] = int(rec['company'])
                rec['operation_center'] = operation_centers[0].id
                rec['payment_term'] = payment_term.id
                if rec['lines']:
                    for ln in rec['lines']:
                        code = ln.pop('test_code')
                        ln['code'] = code
                        exams = Template.search([
                            ('code',  '=', code),
                            ('type',  '=', 'service'),
                            ('salable',  '=', True),
                        ])
                        if not exams:
                            msg['status'] = 'error'
                            msg['msg'] = f'Examen no existe {code}'
                            return
                        ln['test'] = exams[0].products[0].id
                    rec['lines'] = [('create', rec['lines'])]

                copago = rec.get('copago', None)
                cuota_moderadora = rec.get('cuota_moderadora', None)
                discount_amount = rec.get('discount_amount', None)
                if copago and discount_amount:
                    rec['copago'] = Decimal(copago) - Decimal(discount_amount)
                if cuota_moderadora and discount_amount:
                    rec['cuota_moderadora'] = Decimal(
                        cuota_moderadora) - Decimal(discount_amount)

                if rec.get('payments'):
                    for pay in rec['payments']:
                        code = pay['payment_mode']
                        if code != '2':
                            code = '0'  # OTROS MEDIOS NO CLASIFICADOS
                        pay_modes = PaymentMode.search([
                            ('code', '=', code)
                        ])
                        if pay_modes:
                            pay_mode = pay_modes[0]
                            pay['payment_mode'] = pay_mode.id
                            pay['number'] = pay.pop('voucher')
                            pay['payment_date'] = rec['order_date']
                            pay['amount'] = round(Decimal(pay['amount']), 2)
                        else:
                            msg['status'] = 'error'
                            msg['msg'] = f'Modo de pago {code} no existe'
                            return
                    rec['payments'] = [('create', rec['payments'])]

                if patient:
                    rec['patient'] = patient.id

                rec['status_invoice'] = 'without_invoice'
                try:
                    _records = LabOrder.create([rec])
                except Exception as e:
                    msg['response'] = e
                    msg['status'] = 'error'
                    return

                if _records:
                    _record = _records[0]
                    try:
                        # LabOrder.reception(_records)
                        # _method = getattr(LabOrder, 'execute_processing')
                        # _method(LabOrder, args=_record)
                        pass
                    except Exception as e:
                        print('Error desconocido...', e)
                    return _record
            else:
                msg['msg'] = 'Esta intentando crear una orden que ya existe'
                msg['status'] = 'warning'

        try:
            record = _save_lab_order()
            if record:
                _fields = ['number', 'id']
                json_res = jsonify_record(record, _fields)
                msg['record'] = json_res
        except Exception as e:
            print(e)
            msg['response'] = e
            msg['status'] = 'error'

        return send_data(msg)

    #  dos endpoints consulta factura
    @app.route("/search_invoice", methods=['POST'])
    def search_invoice():
        data = json.loads(request.data.decode("utf-8"))
        rec = data['record']
        ctx = data.get('context', {})
        msg = {
            'status': 'ok',
        }
        user = ctx.get('user')

        @tryton.transaction(context=ctx, user=user)
        def _search_invoice():
            if rec.get('order'):
                LabOrder = _pool.get('laboratory.order')
                order = LabOrder.search(['number', '=', rec['number']])
                values = {
                    'order_number': order.number,
                    'order_reference': order.reference,
                    'invoice': order.invoice.number or '',
                    'invoice_state': order.invoice.state or '',
                    'invoice_copago': order.invoice_co_pay.number or '',
                    'invoice_state_copago': order.invoice_co_pay.state or '',
                }

                return values

        try:
            record = _search_invoice()
            if record:
                msg['record'] = record
        except Exception as e:
            print(e)
            abort(500, description="Error in API transaction")

        return send_data(msg)

    @app.route("/create_invoice", methods=['POST'])
    def create_invoice():
        data = json.loads(request.data.decode("utf-8"))
        rec = data['record']
        ctx = data.get('context', {})
        Invoice = _pool.get('account.invoice')
        Party = _pool.get('party.party')
        Template = _pool.get('product.template')
        PaymentTerm = _pool.get('account.invoice.payment_term')
        OperationCenter = _pool.get('company.operation_center')
        Journal = _pool.get('account.journal')
        msg = {
            'status': 'ok',
        }
        user = ctx.get('user')
        print_rec(rec)
        print('_-----Create invoice-----')

        @tryton.transaction(context=ctx, user=user)
        def _create_invoice():
            print('...0....', )
            journal, = Journal.search([
                ('type', '=', 'revenue'),
                ], limit=1)
            print('...0.1....')
            id_number = rec['customer']['id_number']
            parties = Party.search([
                ('id_number', '=', id_number),
            ])
            if parties:
                party = parties[0]
                rec['party'] = party.id
                _ = rec.pop('customer')
            else:
                msg['status'] = 'error'
                msg['msg'] = f'Tercero no encontrado {id_number}'
                return

            number = rec.pop('number')
            invoices = Invoice.search_read([
                ('party', '=', party.id),
                ('reference', '=', number),
                ('type', '=', 'out'),
                ('invoice_date', '=', rec['invoice_date']),
            ], fields_names=['number'])
            if invoices:
                msg['response'] = f'La factura que intenta crear ya existe! {number}'
                msg['status'] = 'error'
                return

            print('...0.2....')
            address = Party.address_get(party, type='invoice')
            if rec.get('patient'):
                # if rec['customer'] != rec['patient']:
                #     patients = Party.search([
                #         ('id_number', '=', rec['patient']['id_number']),
                #     ])
                #     if patients:
                #         patient = patients[0]
                #     else:
                #         msg['status'] = 'error'
                #         msg['msg'] = 'Paciente no encontrado'
                #         return
                # else:
                #     patient = party
                _ = rec.pop('patient')
            else:
                patient = None

            code_oc = OPERATION_CENTER[rec.pop('operation_center')]
            operation_centers = OperationCenter.search([
                ('code', '=', code_oc)
            ])
            print('...1....', operation_centers)
            if not operation_centers:
                msg['status'] = 'error'
                msg['msg'] = 'Centro de operaciones no encontrado'
                return

            op_center = operation_centers[0]
            payment_term, = PaymentTerm.search([
                ('code', '=', rec['payment_term'])
            ])
            _ = rec.pop('list_price_code')
            if not op_center.electronic_authorization_credit:
                msg['status'] = 'error'
                msg['msg'] = f'Este centro de operaciones {code_oc} no tiene asignada una autorizacion de facturacion a credito'
                return

            rec['authorization'] = op_center.electronic_authorization_credit.id
            rec['operation_center'] = op_center.id
            rec['reference'] = number
            rec['payment_term'] = payment_term.id
            rec['journal'] = journal.id
            rec['account'] = party.account_receivable_used.id
            rec['invoice_address'] = address.id
            rec['currency'] = 31
            rec['company'] = 1
            rec['payment_term'] = payment_term.id
            rec['type'] = 'out'
            rec['invoice_type'] = '1'
            invoice_origin = rec.pop('invoice_origin', None)
            if invoice_origin:
                rec['description'] = invoice_origin
            _ = rec.pop('discount_amount', None)
            _ = rec.pop('contract', None)
            _ = rec.pop('orders', None)

            if rec['lines']:
                quantities = []
                for ln in rec['lines']:
                    quantities.append(int(ln['quantity']))
                    code = ln.pop('test_code')
                    exams = Template.search([
                        ('code',  '=', code),
                        ('salable', '=', True),
                        ('type', '=', 'service'),
                    ])
                    if not exams:
                        msg['status'] = 'error'
                        msg['msg'] = f'Examen no existe {code}'
                        return
                    else:
                        product = exams[0].products[0]
                        ln['type'] = 'line'
                        ln['product'] = product.id
                        ln['description'] = product.rec_name
                        ln['account'] = product.template.account_category.account_revenue.id
                        ln['unit'] = product.template.default_uom.id
                        ln['operation_center'] = operation_centers[0].id
                if sum(quantities) < 0:
                    rec['invoice_type'] = '91'

                rec['lines'] = [('create', rec['lines'])]
            print('...3....')
            print('..'*90)
            print(rec)
            print('..'*90)
            try:
                res = Invoice.create([rec])
                print(res)
                print('..validando----------------------')
                if res:
                    return res[0]
            except:
                msg['status'] = 'error'
                msg['msg'] = f'Error en la creacion de factura'

        record = _create_invoice()
        if record:
            if 1:  # try:
                _fields = ['number', 'reference', 'id']
                json_rec = jsonify_record(record, _fields)
                msg['record'] = json_rec
            else:  # except Exception as e:
                print(e)
                msg['status'] = 'error'
                abort(500, description="Error in API transaction")

        return send_data(msg)

    return app
