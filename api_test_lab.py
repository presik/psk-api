import requests
import simplejson as json

# The port is 5090 by default
api_url = 'localhost:5090'
database = 'DEMO60'
# api_url = '107.161.186.130:5090'
# database = 'PASTEUR'

api = '/'.join(['http:/', api_url, database])

ctx = {
    "company": 1,
    "user": 1,
    "token": "XXXXXXXXXXXXXXXXX"
}


def test_search_party():
    body = {
        'model': 'party.party',
        'fields': ['id', 'id_number', 'name'],
        "context": ctx,
        'args': {
            'domain': '[]',
        },
        'context': ctx,
    }
    route = api + '/search'
    return route, body


def test_create_party():
    body = {
        'model': 'party.party',
        'context': ctx,
        'record': {
            'ext_id': '2124',
            'name': 'FRANCY TEJEIRO VIA\u00f1A',
            'id_number': '1096251521',
            'email': 'yeny@gmail.com',
            'regime_tax': 'regimen_no_responsable',
            'fiscal_regimen': '48',
            'obligaciones_fiscales': 'O-23',
            'credit_limit_amount': '2750000',
            'category': '01',
            'address': {
                'street': 'CRA 82 N 10 29',
                'country_code': '169',
                'department_code': '68',
                'city_code': '001',
            },
            'type_document': '13',
            'type_person': 'persona_natural',
            'ciiu_code': '',
            'phone': '536284262',
            'birthday': '2010-10-03',
            'first_name': '',
            'second_name': '',
            'first_family_name': '',
            'second_family_name': '',
        }
    }
    route = api + '/create_party'
    return route, body


def test_update_party():
    body = {
        'model': 'party.party',
        'context': ctx,
        'record': {
            'ext_id': '2123',
            'name': 'ALCALDIA DE ENVIGADO SAS',
            'id_number': '96324221',
            'email': 'gomez@gmail.com',
            'regime_tax': 'regimen_no_responsable',
            'fiscal_regimen': '48',
            'obligaciones_fiscales': 'O-47',
            'credit_limit_amount': '2600000',
            'category': '01',
            'address': {
                'street': 'CALLE 33 N 17 99',
                'country_code': '169',
                'department_code': '68',
                'city_code': '077',
            },
            'type_document': '13',
            'type_person': 'persona_natural',
            'ciiu_code': '9999',
            'phone': '536284265',
            'birthday': '2010-10-03',
            'first_name': '',
            'second_name': '',
            'first_family_name': '',
            'second_family_name': '',
        }
    }
    route = api + '/update_party'
    print('------------------------------------------------')
    print(body)
    return route, body


def test_save_test_type():
    body = {
        'model': 'laboratory.test_type',
        'context': ctx,
        'record': {
            'ext_id': '0735',
            'name': 'ALCOHOL EN SALIVA',
            'code': '1268',
            'cups_code': '190312',
            'active': True,
            'description': '', # Additional info
        }
    }
    route = api + '/save_test_type'
    return route, body


def test_save_lab_order1():
    body = {
        "model": "laboratory.order",
        "context": ctx,
        "record": {
            "number": "2108301359",
            "customer": {
                "id_number": "96324221",
                "ext_id": "294350"
            },
            "description": "Firulais - Cod. 000192",
            "order_date": "2021-08-07",
            "company": 1,
            "list_price_code": "01",
            "operation_center": "8",
            "payment_term": "1",
            "discount_amount": 0,
            "comment": "Aqui se puede agregar la Orden de Servicio de la veterinaria",
            "lines": [
                {
                    "test_code": "18282",
                    "quantity": "1",
                    "unit_price": "11417.16"
                }, {
                    "test_code": "08165",
                    "quantity": "1",
                    "unit_price": "135129.16"
                }
            ],
            "payments": [
                {
                    "payment_mode": "01",
                    "amount": "146546.32",
                    "voucher": "727277777"
                },
            ],
        }
    }
    route = api + '/save_lab_order'
    return route, body


def test_save_lab_order2():
    body = {
        "model": "laboratory.order",
        "context": ctx,
        "record": {
            "number": "00020",
            "customer": {
                "id_number": "8035726522",
                "ext_id": "294350"
            },
            'patient': {
                'id_number': '3081837867',
            },
            "description": " Factura Copago example",
            "order_date": "2021-09-01",
            "company": 1,
            "list_price_code": "01",
            "operation_center": "12",
            "payment_term": "1",
            "discount_amount": 0,
            # "copago": "10000.00",
            "comment": "Aqui se puede agregar la Orden de Servicio",
            "lines": [
                {
                    "test_code": "18282",
                    "quantity": "1",
                    "unit_price": "11417.16"
                }, {
                    "test_code": "08165",
                    "quantity": "1",
                    "unit_price": "135129.16"
                }
            ],
            "payments": [
                # {
                #     "payment_mode": "01",
                #     "amount": "10000",
                #     "voucher": ""
                # },
            ],
        }
    }
    route = api + '/save_lab_order'
    return route, body


def test_save_lab_order3():
    body = {
        "model": "laboratory.order",
        "context": ctx,
        "record": {
            "number": "0000006",
            "customer": {
                "id_number": "96324221",
                "ext_id": "294350"
            },
            'patient': {
                'id_number': '1528974625',
            },
            "description": " Factura example",
            "order_date": "2021-09-01",
            "company": 1,
            "list_price_code": "01",
            "operation_center": "8",
            "payment_term": "1",
            "discount_amount": '46000',
            # "copago": "13500.00",
            "comment": "Aqui se puede agregar la Orden de Servicio",
            "lines": [
                {
                    "test_code": "18282",
                    "quantity": "1",
                    "unit_price": "25000"
                }, {
                    "test_code": "08165",
                    "quantity": "1",
                    "unit_price": "21000"
                }
            ],
            "payments": [
                {
                    "payment_mode": "01",
                    "amount": "38000",
                    "voucher": "X45322"
                },
            ],
        }
    }
    route = api + '/save_lab_order'
    return route, body


def test_create_credit_note():
    body = {
        "model": "account.invoice",
        "context": ctx,
        "record": {
            "number": "0506",
            "customer": {
                "id_number": "96324221",
                "ext_id": "294350"
            },
            'patient': {
                'id_number': '1528974625',
                "ext_id": "294351"
            },
            "description": "Firulais - Cod. 000192",
            "invoice_origin": "00000",
            "invoice_date": "2021-08-30",
            "company": 1,
            "list_price_code": "01",
            "operation_center": "8",
            "payment_term": "1",
            "comment": "Aqui se puede agregar la Orden de Servicio de la veterinaria",
            "lines": [
                {
                    "test_code": "18282",
                    "quantity": "-1",
                    "unit_price": "11417.16"
                }, {
                    "test_code": "08165",
                    "quantity": "-1",
                    "unit_price": "135129.16"
                }
            ],
        }
    }
    route = api + '/create_invoice'
    return route, body


def test_create_invoice():
    body = {
        "model": "account.invoice",
        "context": ctx,
        "record": {
            "number": "1506",
            "customer": {
                "id_number": "8654723890",
                "ext_id": "0003"
            },
            # 'patient': {
            #     'id_number': '1528974625',
            #     "ext_id": "294351"
            # },
            "description": "Firulais - Cod. 000192",
            "contract": "000001",
            "period": "2021-09",
            "orders": ["2678", "2679", "0362", "9710"],
            "invoice_date": "2021-09-01",
            "company": 1,
            "copago": '35600',
            "list_price_code": "01",
            "operation_center": "12",
            "payment_term": "1",
            "comment": "Factura Prepagada",
            "lines": [
                {
                    "test_code": "1268",
                    "quantity": "1",
                    "unit_price": "11417.16"
                }, {
                    "test_code": "1122",
                    "quantity": "1",
                    "unit_price": "135129.16"
                }
            ],
        }
    }
    route = api + '/create_invoice'
    return route, body


def test_update_credit_limit():
    body = {
        'ext_id': '2123',
        'id_number': '6983925821',
        'credit_amount': '100',
        'credit_limit_amount': '140',
    }


def test_save_lab_order_remote():
    body = {
        "model": "laboratory.order",
        "context": ctx,
        "record": {
            'number': '8-2108301517',
            'customer': {
                'id_number': '890903790',
                'ext_id': '215',
                'is_person': 'False'
            },
            'patient': {
                'id_number': '6568421',
                'ext_id': '496411',
                'is_person': 'True'
            },
            'description': '',
            'order_date': '2021-09-01',
            'company': '1',
            'operation_center': '8',
            'payment_term': '1',
            'list_price_code': '01',
            'discount_amount': '0',
            'copago': '25000.00',
            'delivery': None,
            'comment': '',
            'lines': [
                {'test_code': '1', 'quantity': '1', 'unit_price': '3631'},
                {'test_code': '1004', 'quantity': '1', 'unit_price': '5863'},
                {'test_code': '1021', 'quantity': '1', 'unit_price': '63345'},
                {'test_code': '1064', 'quantity': '1', 'unit_price': '7381'}
            ],
            'payments': [
                {'payment_mode': '4', 'amount': '25000.00', 'voucher': 'Z513432'}
            ]
        }
    }
    route = api + '/save_lab_order'
    return route, body


if __name__ == "__main__":
    # This method create a Sale Order (Este metodo crea una venta)

    route, body = test_create_party()
    # route, body = test_save_lab_order_remote()
    data = json.dumps(body)
    result = requests.post(route, data=data)
    print(result)
    values = result.json()
    if isinstance(values, dict):
        for k, v in values.items():
            print(k, ' : ', v)
    else:
        for v in values:
            print('---------------------------------')
            print(v)
