
# MODELS = {
#     'purchase.purchase': {
#         'rec_name': 'number',
#         'fields': [
#             'party', 'number', 'state', 'lines', 'purchase_date', 'create_uid',
#             'description', 'untaxed_amount_cache', 'total_amount_cache',
#             'signed_date', 'sign_approved_link', 'project',
#             'company', 'warehouse', 'invoice_address', 'payment_term',
#             'reference', 'state_string', 'untaxed_amount', 'tax_amount',
#             'total_amount', 'state_string', 'approved_by'
#         ],
#     },
#     'purchase.line': {
#         'rec_name': 'product',
#         'fields': [
#             'product', 'unit_price', 'quantity', 'amount', 'description',
#             'unit'
#         ],
#     },
#     'rental.service': {
#         'rec_name': 'number',
#         'fields': [
#             'party', 'number', 'service_date', 'hiring_time', 'state',
#             'check_list', 'comment', 'start_date', 'deposit_paid', 'booking',
#             'end_date', 'product', 'product', 'booking.number', 'bank_bsb',
#             'bank_account_number', 'odometer_start', 'cover_prices',
#             'customer_contract_agree', 'photo_link_agree', 'early_return',
#             'photo_link_party_id', 'expiration_days_cache', 'mobile', 'email',
#             'issues_presented', 'part_missing', 'odometer_end'
#         ]
#     },
#     'rental.service.product_check_list': {
#         'rec_name': 'rec_name',
#         'fields': [
#             'item', 'kind', 'state_delivery', 'state_return'
#         ]
#     },
#     'rental.booking': {
#         'rec_name': 'number',
#         'fields': [
#             'first_name', 'doc_number', 'last_name', 'mobile', 'email',
#             'number', 'post_code', 'subdivision', 'address', 'city', 'product',
#             'start_date', 'booking_date', 'lapse_time', 'state', 'comment',
#             'end_date', 'type_document', 'category'
#         ]
#     },
#     'surveillance.schedule': {
#         'rec_name': 'rec_name',
#         'fields': [
#             'guard', 'period', 'customer', 'location', 'state'
#         ]
#     },
#     'product.category': {
#         'rec_name': 'name',
#         'fields': ['name', 'leasable', 'parent', 'childs', 'name_icon',
#                    'accounting']
#     },
#     'product.check_list_item': {
#         'rec_name': 'name',
#         'fields': ['name', 'kind']
#     },
#     'sale.line': {
#         'rec_name': 'product',
#         'fields': [
#             'product', 'quantity', 'amount', 'description', 'note', 'discount',
#             'discount', 'order_sended', 'unit_price_w_tax', 'amount_w_tax',
#             'product.description', 'unit.symbol', 'unit.digits',
#             'product.template.sale_price_w_tax', 'product.template.name',
#             'product.code', 'product.sale_price_w_tax', 'qty_fraction',
#         ]
#     },
#     'sale.sale': {
#         'rec_name': 'number',
#         'fields': [
#             'number', 'party', 'salesman.party.name', 'lines', 'sale_date',
#             'state', 'total_amount_cache', 'salesman', 'payment_term',
#             'invoices', 'payments', 'untaxed_amount', 'position', 'tax_amount',
#             'total_amount', 'residual_amount', 'paid_amount', 'invoice',
#             'invoice_address', 'delivery_charge', 'price_list', 'agent', 'commission',
#             'invoice_number', 'invoice.state', 'shipment_address', 'shop',
#             'state_string', 'delivery_party', 'reference', 'waiting_time',
#             'delivery_state', 'kind', 'channel', 'consumer', 'source', 'total_discount'
#         ]
#     },
#     'account.statement.journal': {
#         'rec_name': 'number',
#         'fields': ['name', 'require_voucher', 'kind']
#     },
#     'company.employee': {
#         'rec_name': 'rec_name',
#         'fields': ['rec_name', 'party']
#     },
#     'commission.agent': {
#         'rec_name': 'rec_name',
#         'fields': [
#             'active', 'party', 'party.id_number', 'rec_name', 'plan.percentage'
#         ]
#     },
#     'sale.delivery_party': {
#         'rec_name': 'rec_name',
#         'fields': [
#             'active', 'party', 'party.id_number', 'party.phone', 'rec_name', 'number_plate', 'type_vehicle'
#         ]
#     },
#     'sale.discount': {
#         'rec_name': 'rec_name',
#         'fields': ['active', 'name', 'rec_name', 'discount']
#     },
#     'sale.web_channel': {
#         'rec_name': 'rec_name',
#         'fields': ['id', 'rec_name']
#     },
#     'party.consumer': {
#         'rec_name': 'rec_name',
#         'fields': ['party', 'id_number', 'rec_name', 'name', 'address', 'phone']
#     },
#     'account.invoice.payment_term': {
#         'rec_name': 'name',
#         'fields': ['name', 'active']
#     },
#     'sale.device': {
#         'rec_name': 'name',
#         'fields': [
#             'name', 'shop', 'shop.company', 'shop.name', 'shop.taxes',
#             'shop.party', 'journals', 'shop.product_categories', 'journal',
#             'shop.payment_term', 'shop.warehouse', 'shop.discount_pos_method',
#             'shop.salesman_pos_required', 'shop.electronic_authorization',
#             'shop.invoice_copies', 'shop.pos_authorization', 'shop.discounts',
#             'shop.computer_authorization', 'shop.manual_authorization',
#             'shop.credit_note_electronic_authorization', 'shop.salesmans',
#             'shop.debit_note_electronic_authorization', 'shop.delivery_man',
#         ]
#     },
#     'sale.shop': {
#         'rec_name': 'name',
#         'fields': ['taxes', 'product_categories', 'party', 'invoice_copies',
#                    'warehouse', 'payment_term', 'salesmans', 'delivery_man',
#                    'discounts']
#     },
#     'account.tax': {
#         'rec_name': 'name',
#         'fields': [
#             'name'
#         ]
#     },
#     'company.company': {
#         'rec_name': 'rec_name',
#         'fields': [
#             'party', 'logo'
#         ],
#         'binaries': ['logo']
#     },
#     'res.user': {
#         'rec_name': 'name',
#         'fields': ['name', 'sale_device']
#     },
#     'product.product': {
#         'rec_name': 'rec_name',
#         'fields': [
#             'name', 'code', 'barcode', 'write_date', 'description',
#             'template.sale_price_w_tax', 'template.account_category',
#             'location.name', 'image', 'image_icon', 'quantity',
#             'encoded_sale_price', 'location', 'locations', 'uom',
#             'sale_price_w_tax', 'extra_tax'
#         ],
#         'binaries': ['image']
#     },
#     'party.party': {
#         'rec_name': 'name',
#         'fields': [
#             'name', 'id_number', 'addresses', 'phone', 'customer_payment_term',
#             'customer_payment_term.name', 'credit_limit_amount',
#             'credit_amount', 'street', 'receivable', 'salesman'
#         ]
#     },
#     'ir.action.report': {
#         'rec_name': 'report_name',
#         'fields': ['action', 'report_name']
#     },
#     'sale.configuration': {
#         'rec_name': 'id',
#         'fields': [
#             'tip_product', 'tip_product.code', 'show_description_pos',
#             'show_product_image', 'show_location_pos', 'show_order_number',
#             'show_position_pos', 'show_stock_pos', 'encoded_sale_price',
#             'tip_rate', 'password_force_assign', 'show_agent_pos',
#             'show_brand', 'show_delivery_charge', 'decimals_digits_quantity',
#             'password_admin_pos', 'show_fractions', 'discount_pos_method',
#             'new_sale_automatic', 'default_invoice_type',
#             'cache_products_local', 'print_invoice_payment', 'use_price_list',
#             'show_party_categories',
#         ]
#     },
#     'party.address': {
#         'rec_name': 'name',
#         'fields': [
#             'name', 'street'
#         ]
#     },
#     'ir.module': {
#         'rec_name': 'name',
#         'fields': [
#             'name', 'state'
#         ]
#     },
#     'hotel.booking': {
#         'rec_name': 'number',
#         'fields': [
#             'number', 'booking_date', 'untaxed_amount', 'party', 'state', 'media',
#             'registration_state', 'contact', 'complementary', 'total_amount',
#             'plan', 'invoice_method', 'company', 'price_list', 'payment_term'
#         ]
#     },
#     'hotel.operation': {
#         'rec_name': 'reference',
#         'fields': [
#             'reference', 'start_date', 'end_date', 'party', 'state', 'kind', 'room',
#             'complementary', 'pending_payment', 'total_amount', 'price_list',
#             'accommodation',
#             'main_guest'
#         ]
#     },
#     'crm.customer_service': {
#         'rec_name': 'number',
#         'fields': [
#             'party', 'customer', 'address', 'phone', 'city', 'case', 'state',
#             'description', 'response', 'cs_date', 'effective_date', 'company',
#             'region', 'health_provider', 'receiver', 'department_region', 'open'
#         ]
#     },
#     'crm.case': {
#         'rec_name': 'name',
#         'fields': [
#             'name'
#         ]
#     },
#     'crm.fiduprevisora_department': {
#         'rec_name': 'name',
#         'fields': ['name', 'region']
#     },
#     # 'sale.sale.delivery_party': {
#     #     'rec_name': 'rec_name',
#     #     'fields': ['delivery_party', 'sale', 'state']
#     # },
#     'crm.region_fiduprevisora': {
#         'rec_name': 'name',
#         'fields': ['id', 'name', 'departments']
#     },
#
#     'account.voucher': {
#         'rec_name': 'number',
#         'fields': [
#             'number', 'party', 'bank', 'check_number', 'payment_mode',
#             'payment_type', 'voucher_type', 'voucher_type_string', 'date',
#             'journal', 'currency', 'company', 'lines', 'comment',
#             'description', 'state', 'state_string', 'amount_to_pay',
#             'reference', 'target_account_bank', 'bank_account_number',
#             'approved_by',
#         ]
#     },
#
#     'account.voucher.line': {
#         'rec_name': 'reference',
#         'fields': [
#             'reference', 'detail', 'amount_original', 'amount',
#             'party', 'account',
#         ],
#     },
# }
