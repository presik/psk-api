
# TRYTON INSTALL API

Esta guia describe los métodos para conectarse a la API de Tryton,
creada por Presik SAS.


## Dependences

Install next libraries:

    sudo apt install build-essential
    sudo apt install python3-dev
    sudo apt install libpq-dev
    sudo apt install gunicorn

    pip3 install wheel
    pip3 install psycopg2-binary
    pip3 install simplejson
    pip3 install pycrypto
    pip3 install flask==1.1.4
    pip3 install flask_tryton
    pip3 install flask_cors
    pip3 install gunicorn==20.1.0
    pip3 install Werkzeug==0.15.1

## Configuration

Create a directory in home called ".flask"in Home, for add configuration files.

    mkdir ~/.flask


Add file dash.ini to .flask directory

    cp {MYPATH}/predash/dash.ini ~/.flask/dash.ini


Edit dash.ini and adjust for your company, database, etc.

Test gunicorn is installed starting app from terminal, inside predash_api
directory, (inside virtualenv):

    gunicorn --bind 0.0.0.0:5070 wsgi:app


Test gun_config.py file starting app:

    gunicorn -c gun_config.py wsgi:app

Create predash_api.service file for systemd:

    sudo nano /etc/systemd/system/predash_api.service


Add this text to the predash_api.service file, don't forget to change "User" and
path to "WorkingDirectory" directory

    # Script Server Presik Technologies

    [Unit]
    Description=Predash API Server
    After=network.target

    [Service]
    User=XXXXX
    WorkingDirectory=/home/psk/predash/predash-api
    ExecStart=/home/psk/.virtualenvs/tryton50/bin/gunicorn -c gun_config.py wsgi:app
    #ExecStop=

    [Install]
    WantedBy=multi-user.target


Enable the service

    sudo systemctl enable predash_api.service


Start the service

    sudo systemctl start predash_api.service


Check status

    sudo systemctl status predash_api.service
