# Multiple tools for psk api
import os
import base64
import logging
import types
import configparser
from datetime import date

from Crypto.Cipher import XOR
from flask import jsonify, request
from flask.json import JSONEncoder
import math
import simplejson as json


def get_config(param=None):
    default_dir = os.path.join(os.getenv('HOME'), '.flask')
    config_file = os.path.join(default_dir, 'dash.ini')

    config = configparser.ConfigParser()
    config.read(config_file)
    if param:
        return config.get('General', param)
    return config


def encrypt(key, plaintext):
    cipher = XOR.new(key)
    return base64.b64encode(cipher.encrypt(plaintext))


def send_data(data):
    res = jsonify(data)
    res.headers.add('Access-Control-Allow-Origin', '*')
    res.headers.add('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS')
    res.headers.add('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token')
    return res


def print_rec(rec):
    for k, j in rec.items():
        print(k, ' : ', j)


def is_class(obj):
    if obj.get('__class__'):
        # FIXME: Add datetime, bytes, decimal, too
        if obj['__class__'] == 'date':
            return date(obj['year'], obj['month'], obj['day'])
    return obj


def get_data(decode=False):
    if request.args:
        # From react web
        data = request.args.to_dict()
        if data.get('context'):
            data['context'] = eval(data['context'])
        return data
    elif hasattr(request, 'data') and request.data != b'':
        data = request.data.decode("utf-8")
        return json.loads(data, object_hook=is_class)
    else:
        try:
            return request.json
        except AttributeError:
            logging.warning('Attribute error unknown...!')


def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper


class InvalidAPIUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        super().__init__()
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, types.ModuleType):
                return str(obj)
            elif isinstance(obj, types.MethodType):
                return str(obj)
            elif isinstance(obj, date):
                return obj.isoformat()
            elif isinstance(obj, 'bytes'):
                return base64.b64encode(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)
